import csv
import datetime
import time

class FeatureExtract:

    def extractAll(self):
        path = "6new.csv"
        dataSetNum = 0
        usedGroupMap = {}
        # csv head
        with open('6newtest.csv', 'a+', newline='', encoding='utf-8') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',')
            csvwriter.writerow(
                ['diff_dst_port', 'is_same_as_last_src_ip', 'have_icmp', 'icmp_count', 'avg_icmp_len', 'label'])
        # reader basic csv
        with open(path, 'r', newline='') as csvfile:
            reader = csv.reader(csvfile)
            rows = [row for row in reader]
            timeList = list()
            idList = list()
            startTime = 0
            endTime = 0
            lastSrcIp = ""
            i = 0
            for row in rows:
                if i == 0:
                    i += 1
                    continue
                if len(row) <= 3:
                    continue
                srcIp = row[3]
                dstIp = row[4]
                key = str(srcIp) + "," + str(dstIp)
                if key in usedGroupMap:
                    continue
                else:
                    usedGroupMap[key] = 1
            print(usedGroupMap)

            for key in usedGroupMap:
                self.extractSingle(key, lastSrcIp, rows)
                lastSrcIp = srcIp
            print("data set num: ", dataSetNum)

    def extractSingle(self, standardKey, lastStandardSrcIp, rows):
        rowCount = 0
        diffPortCount = 0
        tcpCount = 0
        udpCount = 0
        icmpCount = 0
        tcpPktLenSum = 0
        udpPktLenSum = 0
        icmpPktLenSum = 0
        haveIcmp = 0
        packetLenSum = 0
        usedDstPortMap = {}
        isSameAsLastSrcIp = 0
        i = 0
        for row in rows:
            if i == 0:
                i += 1
                continue
            if len(row) <= 3:
                continue
            srcIp = row[3]
            dstIp = row[4]
            key = str(srcIp) + "," + str(dstIp)
            if key != standardKey:
                continue
            if lastStandardSrcIp != "" and lastStandardSrcIp == srcIp:
                isSameAsLastSrcIp = 1
            rowCount += 1
            dstPort = row[6]
            if dstPort in usedDstPortMap:
                print()
            else:
                diffPortCount += 1
                usedDstPortMap[dstPort] = 1
            protocol = row[2]
            pktLen = 0 if row[16] is None else row[16]
            if protocol == 'tcp':
                tcpCount += 1
                tcpPktLenSum += int(pktLen)
            elif protocol == 'udp':
                udpCount += 1
                udpPktLenSum += int(pktLen)
            elif protocol == 'icmp':
                haveIcmp = 1
                icmpCount += 1
                icmpPktLenSum += int(pktLen)
            i += 1
            packetLenSum += int(pktLen)

        with open('6newtest.csv', 'a+', newline='', encoding='utf-8') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=',')
            # csvwriter.writerow(['diff_dst_port','is_same_as_last_src_ip','have_icmp','tcp_count','avg_tcp_len',
            #                     'udp_count','avg_udp_len','icmp_count','avg_icmp_len','pkg_len','label'])
            # csvwriter.writerow(['diff_dst_port', 'is_same_as_last_src_ip', 'have_icmp', 'icmp_count', 'avg_icmp_len', 'label'])
            csvwriter.writerow([diffPortCount,
                                isSameAsLastSrcIp,
                                haveIcmp,
                                # tcpCount,
                                # round(tcpPktLenSum / tcpCount, 0) if tcpCount > 0 else 0,
                                # udpCount,
                                # round(udpPktLenSum / udpCount, 0) if udpCount > 0 else 0,
                                icmpCount,
                                round(icmpPktLenSum / icmpCount, 0) if icmpCount > 0 else 0,
                                # packetLenSum / (i - 1) if i > 1 else 0,
                                # placeholder
                                4
            ])

f = FeatureExtract()
f.extractAll()


