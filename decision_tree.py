#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from sklearn.tree import DecisionTreeClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score
from sklearn.tree import plot_tree
import matplotlib.pyplot as plt
import joblib

# Prepare dataset
dataset_log = pd.read_csv('test-6.csv' ,sep= ',')
dataset_log.head()

# Data Preprocessing - 
dataset_log = dataset_log[['diff_dst_port','is_same_as_last_src_ip','have_icmp','icmp_count','avg_icmp_len','label']]

train_ratio = 0.0
val_ratio = 1.0
dataset_len = len(dataset_log)
featurelen = dataset_log.shape[1] # number of columns

# label_encoder = LabelEncoder()
# for i in range(0, featurelen-3):  # minus numerical values
#     dataset_log.iloc[:,i] = label_encoder.fit_transform(dataset_log.iloc[:,i])

# Prepare training and validation set
dataset_train = dataset_log[:int(dataset_len*train_ratio)]
x_train = dataset_train.values[:,0:dataset_train.shape[1]-1]
y_train = dataset_train.values[:,dataset_train.shape[1]-1]

dataset_val = dataset_log[-int(dataset_len*val_ratio):]
x_val = dataset_val.values[:,0:dataset_val.shape[1]-1]
y_val = dataset_val.values[:,dataset_val.shape[1]-1]

# Train model
# clf_gini = DecisionTreeClassifier(criterion = "gini", random_state = 100, max_depth=4, min_samples_leaf=4)
# clf_gini.fit(x_train, y_train)
# joblib.dump(clf_gini, "model.m")
clf_gini = joblib.load("model.m")
y_pred = clf_gini.predict(x_val)

# Show results
# Unknown : 0, Host scan : 1, Port scan : 2, OS scan : 3
print("Accuracy Score: " + str(accuracy_score(y_val, y_pred)))

plot_tree(clf_gini, filled=True)
plt.title("Decision tree trained")
plt.show()

