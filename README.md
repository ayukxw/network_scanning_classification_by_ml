# Network Scanning Classification By Ml
This is official code repository for NTU CI6232 Intrusion Detection Individual Assignment 2, uploading by Liu Xinwei (G2103290E)

## Usage
Run "main.py" (in Python2) to covert CSV to PCAP with underlying features.

Run "FeatureExtract.py" (in Python3) to extract specific features before training.

Run "decision_tree.py" (in Python3) to train model or test data.

*Note: model.m had been pre-trained. Can directly apply it for testing. 
